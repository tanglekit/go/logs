package logs

import (
	"os"

	"github.com/op/go-logging"
)

var LOG_FORMAT = "%{color}[%{level:.4s}] %{time:15:04:05.00} [%{shortpkg}] %{longfunc} -> %{color:reset}%{message}"
var Log *logging.Logger

func Setup(module string, logFormat ...string) {
	format := LOG_FORMAT
	if len(logFormat) != 0 {
		format = logFormat[0]
	}

	Log = logging.MustGetLogger(module)
	backend1 := logging.NewLogBackend(os.Stdout, "", 0)
	logging.SetFormatter(logging.MustStringFormatter(format))
	logging.SetBackend(backend1)
}

func SetLogLevel(module string, logLevel string) {
	level, err := logging.LogLevel(logLevel)
	if err == nil {
		logging.SetLevel(level, module)
	} else {
		Log.Warningf("Could not set log level to %v: %v", logLevel, err)
		Log.Warning("Using default log level")
	}
}
